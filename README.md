### 开发

下载安装 Elixir 开发环境后，克隆代码
```
git clone https://bitbucket.org/suyuxp/T165.git
```

然后修改 config/config.exs 中的数据库配置
```
config :api, Api.Repo,
  adapter: Ecto.Adapters.Postgres,
  database: "cloapdb",
  username: "docker",
  password: "password",
  hostname: "db"
```

初始化数据库
```
cat docker/db.sql | docker exec -i your-db-container psql -Upostgres
```

运行以下命令，即可访问 http://localhost:8880
```
iex -S mix
```


### 部署

采用 docker 部署的方法
```
docker build -t costapply -f docker/Dockerfile .
docker-compose -f docker/docker-compose.yml up -d
```

按 docker-compose.yml 中的配置，可访问 http://localhost:3000
