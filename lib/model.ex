defmodule Model.CostApply do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Poison.Encoder, only: []}
  schema "cost_applies" do
    field :uuid,        :string
    field :user,        :string
    field :account,     :string
    field :money,       :integer
    field :verified,    :boolean, default: false
    timestamps
  end

  def changeset(apply, params \\ :empty) do
    apply
    |> cast(params, ~w(id), ~w(verified))
  end
end


defimpl Poison.Encoder, for: Model.CostApply do
  def encode(apply, options) do
    entity = %{
      id: apply.id,
      uuid: apply.uuid,
      user: %{name: apply.user},
      account: %{name: apply.account},
      money: apply.money,
      verified: apply.verified,
      created: apply.inserted_at |> format_datetime
    }
    Poison.encode!(entity, options)
  end

  defp format_datetime(time) do
    time |> Ecto.DateTime.to_erl |> Timex.DateTime.from |> Timex.format!("{ISO:Extended}")
  end
end