# ================================================================
# Restful APIs
# ================================================================

defmodule Rest.CostApply do
  use Maru.Router
  import Ecto.Query
  alias Api.Repo
  alias Model.CostApply

  # --------------------------------------------------------------
  # GET /?verified=false
  # --------------------------------------------------------------
  params do
    requires :verified, type: Boolean, default: false
  end
  get do
    query = CostApply |> where([a], a.verified == ^params[:verified])
    conn |> json(query |> Repo.all)
  end


  # --------------------------------------------------------------
  # POST /
  # --------------------------------------------------------------
  params do
    group :user,      type: Map do
      requires :name, type: String
    end
    group :account,   type: Map do
      requires :name, type: String
    end
    requires :money,  type: Integer
  end
  post do
    %{account: %{name: account}, user: %{name: user}, money: money} = params
    row = %{
            uuid: Ecto.UUID.generate,
            account: account,
            user: user,
            money: money,
            verified: false
          }
    conn |> json(CostApply |> struct(row) |> Repo.insert!)
  end


  # --------------------------------------------------------------
  # PUT /:id
  # --------------------------------------------------------------
  params do
    requires :id, type: Integer
    requires :verified, type: Boolean, values: [true]
  end
  put "/:id" do
    apply = CostApply |> Repo.get!(params[:id])
    case apply.verified do
      true  -> conn |> put_status(400) |> json(%{reason: :aleady_archived})
      false ->
        changes = %{id: params[:id], verified: true}
        updated = apply |> CostApply.changeset(changes) |> Repo.update!
        conn |> json(updated)
    end
  end
end
