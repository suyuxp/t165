--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.1
-- Dumped by pg_dump version 9.5.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: cost_applies; Type: TABLE; Schema: public; Owner: docker
--

CREATE TABLE cost_applies (
    id integer NOT NULL,
    uuid character varying(255),
    "user" character varying(255),
    account character varying(255),
    money integer,
    verified boolean DEFAULT false,
    inserted_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE cost_applies OWNER TO docker;

--
-- Name: cost_applies_id_seq; Type: SEQUENCE; Schema: public; Owner: docker
--

CREATE SEQUENCE cost_applies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cost_applies_id_seq OWNER TO docker;

--
-- Name: cost_applies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: docker
--

ALTER SEQUENCE cost_applies_id_seq OWNED BY cost_applies.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: docker
--

CREATE TABLE schema_migrations (
    version bigint NOT NULL,
    inserted_at timestamp without time zone
);


ALTER TABLE schema_migrations OWNER TO docker;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: docker
--

ALTER TABLE ONLY cost_applies ALTER COLUMN id SET DEFAULT nextval('cost_applies_id_seq'::regclass);


--
-- Name: cost_applies_pkey; Type: CONSTRAINT; Schema: public; Owner: docker
--

ALTER TABLE ONLY cost_applies
    ADD CONSTRAINT cost_applies_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: docker
--

ALTER TABLE ONLY schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: cost_applies_uuid_index; Type: INDEX; Schema: public; Owner: docker
--

CREATE UNIQUE INDEX cost_applies_uuid_index ON cost_applies USING btree (uuid);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

