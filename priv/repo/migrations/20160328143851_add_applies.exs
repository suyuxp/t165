defmodule Api.Repo.Migrations.AddApplies do
  use Ecto.Migration

  def up do
    create table(:cost_applies) do
      add :uuid,        :string
      add :user,        :string
      add :account,     :string
      add :money,       :integer
      add :verified,    :boolean, default: false
      timestamps
    end
    create unique_index(:cost_applies, [:uuid])
  end

  def down do
    drop table(:cost_applies)
  end
end
